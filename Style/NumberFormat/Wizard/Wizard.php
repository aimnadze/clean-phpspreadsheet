<?php

namespace PhpSpreadsheet\Style\NumberFormat\Wizard;

interface Wizard
{
    public function format(): string;
}
